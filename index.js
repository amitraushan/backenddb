import admin from 'firebase-admin';
import serviceAccount from './serviceAccountKey.json';
import log4js from 'log4js';
import usersdata from './testusersdata.json'
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://hpcm-fdb43.firebaseio.com"
});
const db = admin.database();
log4js.configure({
  appenders: {
    everything: { type: 'file', filename: 'EmailAndPassword.log' }
  },
  categories: {
    default: { appenders: [ 'everything' ], level: 'debug' }
  }
});

const logger = log4js.getLogger();

function passwordGenerator() {
  const specials = '@#$%';
  const lowercase = 'abcdefghijklmnopqrstuvwxyz';
  const uppercase = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  const numbers = '0123456789';
  const allChar = lowercase + uppercase + numbers;
  let randomString = '';
  let text = '';
  randomString += specials.charAt(Math.floor(Math.random() * specials.length));
  randomString += uppercase.charAt(Math.floor(Math.random() * uppercase.length));
  randomString += lowercase.charAt(Math.floor(Math.random() * lowercase.length));
  randomString += numbers.charAt(Math.floor(Math.random() * numbers.length));
  for (let i = 0; i < 5; i++) {
    text += allChar.charAt(Math.floor(Math.random() * allChar.length));
  }

  function randomsort(a, b) {
    return Math.random() > .5 ? -1 : 1;
  }
  randomString += text;
  let randomStr = randomString.split('').sort(randomsort);
  let password = randomStr.join('')
  return password;
};

for (let i = 0; i < usersdata.length; i++) {
  const userEmailId = usersdata[i].email;
  const password = passwordGenerator();
  const data = {
    useremail : userEmailId,
    password : password 
  }
  logger.debug(data);
  admin.auth().createUser({
      email: userEmailId,
      emailVerified: true,
      password: password
    })
    .then(function (userRecord) {
      let uid = userRecord.uid;
      db.ref(`users/${uid}`).set(usersdata[i]);
      console.log("Successfully created new user:", userRecord.uid);
    })
    .catch(function (error) {
      console.log("Error creating new user:", error);
    });
}

